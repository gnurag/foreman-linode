require 'rake/testtask'

# Tasks
namespace :foreman_linode do
  namespace :example do
    desc 'Example Task'
    task task: :environment do
      # Task goes here
    end
  end
end

# Tests
namespace :test do
  desc 'Test ForemanLinode'
  Rake::TestTask.new(:foreman_linode) do |t|
    test_dir = File.join(File.dirname(__FILE__), '../..', 'test')
    t.libs << ['test', test_dir]
    t.pattern = "#{test_dir}/**/*_test.rb"
    t.verbose = true
    t.warning = false
  end
end

namespace :foreman_linode do
  task :rubocop do
    begin
      require 'rubocop/rake_task'
      RuboCop::RakeTask.new(:rubocop_foreman_linode) do |task|
        task.patterns = ["#{ForemanLinode::Engine.root}/app/**/*.rb",
                         "#{ForemanLinode::Engine.root}/lib/**/*.rb",
                         "#{ForemanLinode::Engine.root}/test/**/*.rb"]
      end
    rescue
      puts 'Rubocop not loaded.'
    end

    Rake::Task['rubocop_foreman_linode'].invoke
  end
end

Rake::Task[:test].enhance ['test:foreman_linode']

load 'tasks/jenkins.rake'
if Rake::Task.task_defined?(:'jenkins:unit')
  Rake::Task['jenkins:unit'].enhance ['test:foreman_linode', 'foreman_linode:rubocop']
end
