require File.expand_path('../lib/foreman_linode/version', __FILE__)
require 'date'

Gem::Specification.new do |s|
  s.name        = 'foreman_linode'
  s.version     = ForemanLinode::VERSION
  s.date        = Time.zone.today
  s.authors     = ['Anurag Patel']
  s.email       = ['gnurag@gmail.com']
  s.homepage    = 'https://gitlab.com/gnurag/foreman-linode'
  s.summary     = 'Foreman compute resource plugin for Linode'
  # also update locale/gemspec.rb
  s.description = 'Foreman compute resource plugin for Linode'

  s.files = Dir['{app,config,db,lib,locale}/**/*'] + ['LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir['test/**/*']

  s.add_dependency 'deface'
  s.add_development_dependency 'rubocop'
  s.add_development_dependency 'rdoc'
end
